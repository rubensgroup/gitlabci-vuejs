# Gitlab CI
Example to use GitlabCI in a NEW projecto or existent, if you have an existent repo, go to `Step 2`

[[_TOC_]]

## Step 1:
Create a project with vuejs to push this code to Gitlab Cloud, if it doesn't exist.
```sh
docker run --rm -v "${PWD}:/$(basename `pwd`)" -w "/$(basename `pwd`)" -it vuejs/ci:latest sh -c "yarn global add @vue/cli && vue create ."
```
In this example we selected **Vuejs3** and **Yarn**</p></li>
### Step 1.1:
Create a Docker File `Dockerfile`
```yaml
# develop stage
FROM  vuejs/ci:latest as develop-stage
WORKDIR /app
COPY package*.json ./
RUN yarn install
COPY . .

# build stage
FROM develop-stage as build-stage
RUN yarn build

# production stage
FROM nginx:1.15.7-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
```
### Step 1.1.2:
Review this project in local with Docker:
```sh
#Buil the image
docker build -t dockerize-gitlabci-vuejs .
```
### Step 1.1.3:
```sh
#Run the docker container
docker run -it -p 8000:80 --rm dockerize-gitlabci-vuejs
```

### Step 1.2:
Create a docker compose file `docker-compose.yml`
```yaml
# for local development
version: '3.7'
services:
  frontend:
    build:
      context: .
      target: 'develop-stage'
    ports:
    - '8000:80'
    volumes:
    - '.:/app'
    command: /bin/sh -c "yarn serve"
```
### Step 1.2.1:
Review this project in local with Docker:
```sh
#Buil the image
docker-compose up --build
```
### Step 1.2.3:
In case you want to add another node package, just run the following command
```sh
docker-compose exec frontend yarn add <package-name>
```

## Step 1.3:
Review this project in local, copy this in your brower `http://0.0.0.0:8080/`


## Step 2:
Create the `.gitlab-ci.yml`in your project.
```yaml
pages:
 image: vuejs/ci:latest
 stage: deploy
 script:
  - npm install --progress=false
  - npm run build
  - rm -rf public
  - mkdir public
  - cp -r dist/* public
 artifacts:
  expire_in: 1 day
  paths:
  - public
 only:
  - master
```
## Step 2.1:
Create the file to config vuejs to use project url `vue.config.js`.
```js
module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/<name-proyect>/'
    : '/'
}
```
## Step 2.2:
Push the changues to branch master in gitlab
```sh
git add .
git commit -m "Add CI and URL Vuejs configuration files"
git push --set-upstream origin master
```



**References:**
- [Vue with Docker; initialize, develop and build](https://medium.com/@jwdobken/vue-with-docker-initialize-develop-and-build-51fad21ad5e6)
- [Vue.js on GitLab Pages](https://knasmueller.net/vue-js-on-gitlab-pages)